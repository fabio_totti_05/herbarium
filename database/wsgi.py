"""
WSGI configuration for uWSGI. Exposes the callable ``application``.
"""
from app import app as application

if __name__ == '__main__':
    application.run()
