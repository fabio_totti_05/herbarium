Online Database
===============

Web-based database implemented using Python's Flask web framework.

Development
-----------

After cloning this repository go into the `database` folder:

```console
git clone git@gitlab.com:fabio05/herbarium.git
cd herbarium/database
```

Install dependencies:

```console
pipenv install
```

Start the Python application with:

```console
pipenv run python3 app.py
```

Then click on the link displayed in your terminal to open the database
plant list in your web browser.

Deployment
----------

The web application runs on [DjangoEurope](https://djangoeurope.com/) using
configuration from the [.djangoeurope](../.djangoeurope) folder.

BasicAuth is configured in _ControlPanel_ > _Websites_ > [herbarium.bittner.it](
https://herbarium.bittner.it) > _Location list_ > _Web user_
