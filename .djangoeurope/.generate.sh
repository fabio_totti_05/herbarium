#!/bin/bash -e
# Taken and adapted from: https://panel.djangoeurope.com/support/doc/flask-uwsgi

export SOCKET_FILE=/run/user/3919/5fada7ca-3cbf-4408-a664-5faa15e2a2e9.sock
export PROJECT_NAME=herbarium
export VENV_HOME=$HOME/.virtualenvs/$PROJECT_NAME

mkdir -pv ~/$PROJECT_NAME/.djangoeurope/init

cd ~/$PROJECT_NAME/.djangoeurope

cat > requirements.txt << EOF
flask>=2
sentry-sdk[flask]
uwsgi
pylatex
EOF

virtualenv -p /usr/bin/python3 $VENV_HOME

$VENV_HOME/bin/pip install -r requirements.txt

wget https://templates.wservices.ch/uwsgi/flask-uwsgi.ini -O uwsgi.ini

wget https://templates.wservices.ch/uwsgi/init -O init/$PROJECT_NAME

sed -i -e 's/PROJECTNAME/'$PROJECT_NAME'/g' -e 's+SOCKET_FILE+'$SOCKET_FILE'+g' init/$PROJECT_NAME

chmod 750 init/$PROJECT_NAME

ln -sv ~/$PROJECT_NAME/.djangoeurope/init/$PROJECT_NAME ~/init/$PROJECT_NAME
